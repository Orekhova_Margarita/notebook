#ifndef NOTEADDITION_H
#define NOTEADDITION_H

#include <QDialog>
#include <QListWidget>
#include "note.h"
#include "base.h"
#include "notebook.h"

namespace Ui {
class noteAddition;
}

class noteAddition : public QDialog
{
    Q_OBJECT

public:
    explicit noteAddition(QWidget *parent = nullptr);
    ~noteAddition();
    Note*nt=nullptr;
    Base* editnt=nullptr;//=new Note;

    QListWidget* qlwn=nullptr;

    QListWidgetItem* qlwnit=nullptr;

    Notebook* noteBook=nullptr;
    void setNoteData();


private slots:
    void on_addNoteAgree_clicked();

private:
    Ui::noteAddition *ui;
};

#endif // NOTEADDITION_H
