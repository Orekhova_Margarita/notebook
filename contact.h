#ifndef CONTACT_H
#define CONTACT_H


#include "base.h"
#include "phone.h"
#include "json-develop/single_include/nlohmann/json.hpp"
//#include "F:\JSON_LIB\json-develop\single_include\nlohmann\json.hpp"
#include <iostream>
#include <string>
#include <fstream>

using json = nlohmann::json;
using namespace std;
class Contact :
    public Base
{
public:
    Contact();
    string name;
    string surname;
    vector<Phone*>numbers;

    virtual void readFromFile(json*newobj);
    virtual void writeToFile(ofstream&o);

    void print();
    void addRecord();
    ~Contact();
};



#endif // CONTACT_H
