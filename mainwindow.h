#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QListWidgetItem>
#include <QListWidget>
#include "notebook.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Notebook*nBook=nullptr;
    QListWidgetItem*someItem=nullptr;

private slots:
    void on_contAddButton_clicked();

    void on_noteAddButton_clicked();

    void on_noteListWidget_itemClicked(QListWidgetItem *item);

    void on_contactListWidget_itemClicked(QListWidgetItem *item);

    void on_contactListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_noteListWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_noteEdit_clicked();

    void on_noteDelete_clicked();

    void on_contEdit_clicked();

    void on_contDelete_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
