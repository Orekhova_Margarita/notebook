#include "noteaddition.h"
#include "ui_noteaddition.h"
#include "notebook.h"
#include "note.h"
noteAddition::noteAddition(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::noteAddition)
{
    ui->setupUi(this);
}

noteAddition::~noteAddition()
{
    delete ui;
}

void noteAddition::on_addNoteAgree_clicked()
{
    if(ui->addNoteAgree->text().toStdString()=="Add")
        {
            nt=new Note;
            nt->title=ui->titleEdit->text().toStdString();
            nt->body=ui->bodyEdit->toPlainText().toStdString();
            noteBook->records.push_back(nt);
            qlwn->addItem(QString::fromUtf8(nt->title.c_str()));
        }

    else if (ui->addNoteAgree->text().toStdString()=="Edit")
        {
            static_cast<Note*>(editnt)->title=ui->titleEdit->text().toStdString();
            static_cast<Note*>(editnt)->body=ui->bodyEdit->toPlainText().toStdString();

            qlwnit->setText(QString::fromUtf8( static_cast<Note*>(editnt)->title.c_str()));

        }

        noteBook->in();

        this->destroy();
}

void noteAddition::setNoteData()
{
    ui->addNoteAgree->setText("Edit");
    ui->titleEdit->setText(QString::fromUtf8((static_cast<Note*>(editnt))->title.c_str()));
    ui->bodyEdit->setText(QString::fromUtf8((static_cast<Note*>(editnt))->body.c_str()));
}
