#include "contaddition.h"
#include "ui_contaddition.h"
#include "notebook.h"
#include "contact.h"
#include "phone.h"

contAddition::contAddition(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::contAddition)
{
    ui->setupUi(this);
}

contAddition::~contAddition()
{
    delete ui;
}


void contAddition::contNumbersAdd(Contact*cont)
{
    if(ui->mobPhone->text().toStdString()!="")
    {
        ph=new Phone;
        ph->type="mobile";
        ph->number=ui->mobPhone->text().toStdString();
        cont->numbers.push_back(ph);
    }
    if(ui->homePhone->text().toStdString()!="")
    {
        ph=new Phone;
        ph->type="home";
        ph->number=ui->homePhone->text().toStdString();
        cont->numbers.push_back(ph);
    }
    if(ui->workPhone->text().toStdString()!="")
    {
        ph=new Phone;
        ph->type="work";
        ph->number=ui->workPhone->text().toStdString();
        cont->numbers.push_back(ph);
    }
}



void contAddition::on_addButton_clicked()
{
    if(ui->addButton->text().toStdString()=="Add")
    {
        ct=new Contact;

        ct->name=ui->nameEdit->text().toStdString();
        ct->surname=ui->surnameEdit->text().toStdString();
        contNumbersAdd(ct);
        noteBook->records.push_back(ct);
        qlwc->addItem(QString::fromUtf8(ct->name.c_str()));

    }

    else if(ui->addButton->text().toStdString()=="Edit")
    {
        static_cast<Contact*>(editct)->name=ui->nameEdit->text().toStdString();
        static_cast<Contact*>(editct)->surname=ui->surnameEdit->text().toStdString();
        static_cast<Contact*>(editct)->numbers.clear();
        contNumbersAdd(static_cast<Contact*>(editct));

        qlwcit->setText(QString::fromUtf8( static_cast<Contact*>(editct)->name.c_str()));

    }


    noteBook->in();

    this->destroy();


}

void contAddition::setContData()
{
    ui->addButton->setText("Edit");
    ui->nameEdit->setText(QString::fromUtf8(static_cast<Contact*>(editct)->name.c_str()));
    ui->surnameEdit->setText(QString::fromUtf8(static_cast<Contact*>(editct)->surname.c_str()));

    for(unsigned i=0;i<static_cast<Contact*>(editct)->numbers.size();i++)
    {
        if(static_cast<Contact*>(editct)->numbers[i]->type=="mobile")
            ui->mobPhone->setText(QString::fromUtf8(static_cast<Contact*>(editct)->numbers[i]->number.c_str()));
        if(static_cast<Contact*>(editct)->numbers[i]->type=="home")
            ui->homePhone->setText(QString::fromUtf8(static_cast<Contact*>(editct)->numbers[i]->number.c_str()));
        if(static_cast<Contact*>(editct)->numbers[i]->type=="work")
            ui->workPhone->setText(QString::fromUtf8(static_cast<Contact*>(editct)->numbers[i]->number.c_str()));

    }

}



