#ifndef NOTE_H
#define NOTE_H


#include "base.h"
#include "json-develop/single_include/nlohmann/json.hpp"
//#include "F:\JSON_LIB\json-develop\single_include\nlohmann\json.hpp"
#include <iostream>
#include <string>
#include <fstream>

using json = nlohmann::json;

using namespace std;
class Note :
    public Base
{
public:
    Note();
    string title;
    string body;

    virtual void readFromFile(json*newobj);
    virtual void writeToFile(ofstream&o);

    void print();
    void addRecord();
    ~Note();
};



#endif // NOTE_H
