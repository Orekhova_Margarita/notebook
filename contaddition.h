#ifndef CONTADDITION_H
#define CONTADDITION_H

#include <QDialog>
#include <QListWidget>
#include "notebook.h"
#include "contact.h"
#include "phone.h"
#include "mainwindow.h"

namespace Ui {
class contAddition;
}

class contAddition : public QDialog
{
    Q_OBJECT

public:
    explicit contAddition(QWidget *parent = nullptr);
    ~contAddition();
    Notebook*noteBook=nullptr;
    Contact*ct=nullptr;
    Phone*ph=nullptr;

    QListWidget* qlwc=nullptr;

    QListWidgetItem* qlwcit=nullptr;

    Base* editct=nullptr;
    void setContData();
    void contNumbersAdd(Contact*cont);



private slots:
    void on_addButton_clicked();

private:
    Ui::contAddition *ui;
};

#endif // CONTADDITION_H
