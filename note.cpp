#include "note.h"
#include "json-develop/single_include/nlohmann/json.hpp"

//#include "F:\JSON_LIB\json-develop\single_include\nlohmann\json.hpp"
#include <fstream>
#include <iomanip>


using json = nlohmann::json;

Note::Note()
{
    this->recordType = "note";
}


Note::~Note()
{
}


void Note::writeToFile(ofstream&o)
{
    json note = { {"record type",recordType},
        {"title",title},
    {"body",body}
    };
    o << setw(4) << note << endl;

}

void Note::print()
{
    std::cout << "title: " << title << endl << "body: " << body << endl;
}

void Note::addRecord() {
    cout << endl << "Input the note's title: ";
    cin >> title;
    cout << endl << "Input the note's body: ";
    cin >> body;
}
void Note::readFromFile(json*newobj) {
    body = newobj->at("body").get<string>();
    title = newobj->at("title").get<string>();
}

