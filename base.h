#ifndef BASE_H
#define BASE_H
#include "json-develop/single_include/nlohmann/json.hpp"
//#include "F:\JSON_LIB\json-develop\single_include\nlohmann\json.hpp"
#include <fstream>
#include <iostream>
#include <string>


using namespace std;
using json = nlohmann::json;

class Base
{
public:
    Base();
    string recordType;

    virtual void readFromFile(json*newobj)=0;
    virtual void writeToFile(ofstream&o) = 0;

    virtual void print() = 0;
    virtual void addRecord() = 0;
    ~Base();
};

#endif // BASE_H
