#ifndef NOTEINFO_H
#define NOTEINFO_H

#include <QDialog>
#include "note.h"

namespace Ui {
class noteInfo;
}

class noteInfo : public QDialog
{
    Q_OBJECT

public:
    explicit noteInfo(QWidget *parent = nullptr);
    ~noteInfo();
    void noteInfoInit(Note*ntptr);

private:
    Ui::noteInfo *ui;
};

#endif // NOTEINFO_H
