#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "contaddition.h"
#include "noteaddition.h"
#include "noteinfo.h"
#include "continfo.h"
#include "contact.h"
#include "note.h"
#include "base.h"
#include "serializer.h"
#include "notebook.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
ui->setupUi(this);
    nBook=new Notebook;
    nBook->out();


    QStringList contactList, noteList;

        for(unsigned i=0; i<nBook->records.size();i++)
        {
            if(nBook->records[i]->recordType=="contact")
                contactList.append(QString::fromUtf8(static_cast<Contact*>(nBook->records[i])->name.c_str()));

            else if(nBook->records[i]->recordType=="note")
                noteList.append(QString::fromUtf8(static_cast<Note*>(nBook->records[i])->title.c_str()));
        }

        ui->contactListWidget->addItems(contactList);
        ui->noteListWidget->addItems(noteList);



    ui->contEdit->setDisabled(1);
    ui->contDelete->setDisabled(1);
    ui->noteEdit->setDisabled(1);
    ui->noteDelete->setDisabled(1);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_contAddButton_clicked()
{
    contAddition*contAddForm=new contAddition(this);
    contAddForm->noteBook=nBook;
    contAddForm->qlwc=ui->contactListWidget;
    contAddForm->show();

}

void MainWindow::on_noteAddButton_clicked()
{
    noteAddition*noteAddForm=new noteAddition(this);
    noteAddForm->noteBook=nBook;
    noteAddForm->qlwn=ui->noteListWidget;
    noteAddForm->show();
}

void MainWindow::on_noteListWidget_itemClicked(QListWidgetItem *item)
{
    ui->noteEdit->setEnabled(1);
    ui->noteDelete->setEnabled(1);
    someItem=item;
}

void MainWindow::on_contactListWidget_itemClicked(QListWidgetItem *item)
{
    ui->contEdit->setEnabled(1);
    ui->contDelete->setEnabled(1);
    someItem=item;

}

void MainWindow::on_noteListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    noteInfo*noteIForm = new noteInfo(this);
    Note *nt;

    for(unsigned i=0; i<nBook->records.size();i++)
        {
            nt=static_cast<Note*>(nBook->records[i]);
            if((nt->title)==item->text().toStdString())
            {
                noteIForm->noteInfoInit(nt);
                break;
            }
        }

    noteIForm->show();
}

void MainWindow::on_contactListWidget_itemDoubleClicked(QListWidgetItem *item)
{
    contInfo*contIForm=new contInfo(this);
    Contact *ct;

    for(unsigned i=0;i<nBook->records.size();i++)
        {
            ct=static_cast<Contact*>(nBook->records[i]);
            if ((ct->name)==item->text().toStdString())
            {
                contIForm->contInfoInit(ct);
                break;
            }
        }

    contIForm->show();
}



void MainWindow::on_noteEdit_clicked()
{
    if(ui->noteListWidget->isItemSelected(someItem))
        {
            noteAddition* noteEditF = new noteAddition(this);
            noteEditF->noteBook=nBook;

            noteEditF->qlwn=ui->noteListWidget;
            noteEditF->qlwnit=someItem;


            for(unsigned i=0; i<nBook->records.size();i++)
            {
                if(static_cast<Note*>(nBook->records[i])->title==someItem->text().toStdString())
                {
                    noteEditF->editnt =nBook->records[i];
                    break;
                }
            }
            noteEditF->setNoteData();
            noteEditF->show();
        }
}

void MainWindow::on_contEdit_clicked()
{
    if(ui->contactListWidget->isItemSelected(someItem))
        {
            contAddition* contEditF = new contAddition(this);
            contEditF->noteBook=nBook;

            contEditF->qlwc=ui->contactListWidget;
            contEditF->qlwcit=someItem;


            for(unsigned i=0; i<nBook->records.size();i++)
            {
                if(static_cast<Contact*>(nBook->records[i])->name==someItem->text().toStdString())
                {
                    contEditF->editct =nBook->records[i];               
                    break;
                }
            }
            contEditF->setContData();
            contEditF->show();
        }

}


void MainWindow::on_noteDelete_clicked()
{
    if(ui->noteListWidget->isItemSelected(someItem))
    {

        for(unsigned j=0;j<nBook->records.size();j++)
        {
            if(static_cast<Note*>(nBook->records[j])->title==someItem->text().toStdString())
                nBook->deleteRec(nBook->records[j]);
        }

        nBook->in();
        delete someItem;

    }

}

void MainWindow::on_contDelete_clicked()
{
    if(ui->contactListWidget->isItemSelected(someItem))
    {      
        for(unsigned j=0;j<nBook->records.size();j++)
        {
            if(static_cast<Contact*>(nBook->records[j])->name==someItem->text().toStdString())
                nBook->deleteRec(nBook->records[j]);
        }

        nBook->in();
        delete someItem;

    }
}
