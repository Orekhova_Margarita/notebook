#include "noteinfo.h"
#include "ui_noteinfo.h"

noteInfo::noteInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::noteInfo)
{
    ui->setupUi(this);
}

noteInfo::~noteInfo()
{
    delete ui;
}
void noteInfo::noteInfoInit(Note*ntptr)
{
    ui->titleText->setText(QString::fromUtf8(ntptr->title.c_str()));
    ui->bodyText->setText(QString::fromUtf8(ntptr->body.c_str()));
    ui->titleText->setDisabled(1);
    ui->bodyText->setDisabled(1);
}
