#ifndef SERIALIZER_H
#define SERIALIZER_H
#include"base.h"
#include "contact.h"
#include "note.h"
#include <vector>

class Serializer
{
public:
    Serializer();
    static void fromJsonFormat(vector<Contact>&contacts, vector<Note>&notes, vector<Base*>&records);
    ~Serializer();
};

#endif // SERIALIZER_H
