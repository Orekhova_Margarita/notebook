#ifndef NOTEBOOK_H
#define NOTEBOOK_H


#include "base.h"
#include <iostream>
#include <string>
#include <vector>
#include "contact.h"
#include "note.h"
using namespace std;
class Notebook
{
public:
    Notebook();
    vector<Base*>records;
    vector <Contact> contacts;
    vector <Note> notes;

    void in();
    void out();

    void showAllRecords();
    void deleteRec(Base*del);

    ~Notebook();
};


#endif // NOTEBOOK_H
