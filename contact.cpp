#include "contact.h"
#include "json-develop/single_include/nlohmann/json.hpp"

//#include "F:\JSON_LIB\json-develop\single_include\nlohmann\json.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include "phone.h"

using namespace std;
using json = nlohmann::json;

Contact::Contact()
{
    this->recordType = "contact";
}


Contact::~Contact()
{
}
void Contact::readFromFile(json*newobj) {
    name = newobj->at("name").get<string>();
    surname = newobj->at("surname").get<string>();
    unsigned k = newobj->at("phones").size();
    if (k > 0) {

        Phone*ph = nullptr;
        for (unsigned i = 0; i < k; i++) {
            ph = new Phone;
            ph->type = newobj->at("phones")[i].at("type").get<string>();
            ph->number = newobj->at("phones")[i].at("number").get<string>();
            numbers.push_back(ph);
        }

    }
}


void Contact::writeToFile(ofstream&o)
{
    json ph;
    json pharray = json::array();
    for (unsigned i = 0; i < numbers.size(); i++)
    {
        ph = { {"type",numbers[i]->type},
       {"number",numbers[i]->number} };
        pharray.push_back(ph);
    }

    json contact = { {"record type",recordType},
        {"name",name},
        {"surname",surname},
        {"phones",pharray}
    };
    o << setw(4) << contact << endl;

}

void Contact::print()
{
    cout <<"name: "<< name <<endl<<"surname: " << surname << endl;
    for (unsigned i = 0; i < numbers.size(); i++)
        numbers[i]->print();

}

void Contact::addRecord(){
    cout << "Input the name: ";
    cin >> name;
    cout << "Input the surname: ";
    cin >> surname;
    char answ;
    cout << "Would you like to add phone numbers? (1 for \"yes\", 0 for \"no\"): ";
    cin >> answ;
    if (answ == '1')
        do{
            Phone*ph = new Phone;
            ph->addRecord();
            numbers.push_back(ph);
            cout << endl << "Continue? (\"enter\" for \"yes\",\"0\" for \"no\")" << endl;
            getchar();
        } while (getchar() != '0');

}




