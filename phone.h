#ifndef PHONE_H
#define PHONE_H


#include "base.h"
#include "json-develop/single_include/nlohmann/json.hpp"
//#include "F:\JSON_LIB\json-develop\single_include\nlohmann\json.hpp"
#include <iostream>
#include <string>
#include <fstream>

using json = nlohmann::json;

using namespace std;
class Phone :
    public Base
{
public:
    Phone();
    string type;
    string number;

    void readFromFile(json*newobj);
    void writeToFile(ofstream&o);

    void print();
    void addRecord();
    ~Phone();
};



#endif // PHONE_H
