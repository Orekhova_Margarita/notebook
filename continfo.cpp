#include "continfo.h"
#include "ui_continfo.h"
#include "notebook.h"
#include <iostream>
using namespace std;

contInfo::contInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::contInfo)
{
    ui->setupUi(this);
}

contInfo::~contInfo()
{
    delete ui;
}
void contInfo::contInfoInit(Contact*ctptr)
{
    ui->nameInfo->setText(QString::fromUtf8(ctptr->name.c_str()));
    ui->surnameInfo->setText(QString::fromUtf8(ctptr->surname.c_str()));

    QStringList phoneList;
    for(unsigned i=0; i<ctptr->numbers.size();i++)
        phoneList.append(QString::fromUtf8(ctptr->numbers[i]->type.c_str()) + ": " + QString::fromUtf8(ctptr->numbers[i]->number.c_str()));

    ui->phones->addItems(phoneList);
    ui->nameInfo->setDisabled(1);
    ui->surnameInfo->setDisabled(1);
    ui->phones->setDisabled(1);
}
