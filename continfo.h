#ifndef CONTINFO_H
#define CONTINFO_H

#include <QDialog>
#include <QListWidgetItem>
#include "notebook.h"
#include "contact.h"


namespace Ui {
class contInfo;
}

class contInfo : public QDialog
{
    Q_OBJECT

public:
    explicit contInfo(QWidget *parent = nullptr);
    ~contInfo();
    void contInfoInit(Contact*ctptr);

private:
    Ui::contInfo *ui;
};

#endif // CONTINFO_H

